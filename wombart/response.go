package wombart

import (
	"fmt"
	"net/http"
)

func wrapResponse(resp *http.Response, err error) (*http.Response, error) {
	if err != nil {
		return resp, err
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return nil, fmt.Errorf("response status code: %d", resp.StatusCode)
	}

	return resp, nil
}
