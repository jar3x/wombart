package wombart

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/pkg/errors"
)

func (client *Client) createTaskID() (string, error) {
	client.mutex.RLock()
	defer client.mutex.RUnlock()
	req, err := http.NewRequest(
		http.MethodPost,
		"https://app.wombo.art/api/tasks",
		strings.NewReader("{\"premium\":false}"),
	)
	req.Header.Set("Authorization", fmt.Sprintf("bearer %s", client.idToken))
	if err != nil {
		return "", errors.Wrap(err, "create request")
	}
	resp, err := wrapResponse(client.httpClient.Do(req))
	if err != nil {
		return "", errors.Wrap(err, "send request")
	}
	result := struct {
		ID string `json:"id"`
	}{}
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return "", errors.Wrap(err, "unmarshal body")
	}

	return result.ID, nil
}
