package wombart

import (
	"net/http"
	"sync"

	"github.com/pkg/errors"
)

type Client struct {
	httpClient *http.Client
	idToken    string
	mutex      sync.RWMutex
}

func NewClient() (*Client, error) {
	client := &Client{
		httpClient: &http.Client{},
	}

	if err := client.refreshIDToken(); err != nil {
		return nil, errors.Wrap(err, "refresh id token")
	}

	return client, nil
}
