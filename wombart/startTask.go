package wombart

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/pkg/errors"
)

func (client *Client) StartTask(taskID string, task TaskSpec) error {
	client.mutex.RLock()
	defer client.mutex.RUnlock()
	b, err := json.Marshal(struct {
		InputSpec TaskSpec `json:"input_spec"`
	}{InputSpec: task})
	if err != nil {
		return errors.Wrap(err, "marshal request body")
	}
	req, err := http.NewRequest(
		http.MethodPut,
		fmt.Sprintf("https://app.wombo.art/api/tasks/%s", taskID),
		strings.NewReader(string(b)),
	)
	req.Header.Set("Authorization", fmt.Sprintf("bearer %s", client.idToken))
	if err != nil {
		return errors.Wrap(err, "create request")
	}

	if _, err := wrapResponse(client.httpClient.Do(req)); err != nil {
		return errors.Wrap(err, "send request")
	}

	return nil
}
