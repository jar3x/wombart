package wombart

type TaskSpec struct {
	Prompt           string    `json:"prompt"`
	Style            TaskStyle `json:"style"`
	DisplayFrequency int       `json:"display_freq"`
}

type TaskStyle int

const (
	TaskStyleSynthwave    = TaskStyle(1)
	TaskStyleUkiyoe       = TaskStyle(2)
	TaskStyleNoStyle      = TaskStyle(3)
	TaskStyleSteamPunk    = TaskStyle(4)
	TaskStyleFantasyArt   = TaskStyle(5)
	TaskStyleVibrant      = TaskStyle(6)
	TaskStyleHD           = TaskStyle(7)
	TaskStylePastel       = TaskStyle(8)
	TaskStylePsychic      = TaskStyle(9)
	TaskStyleDarkFantasy  = TaskStyle(10)
	TaskStyleMystical     = TaskStyle(11)
	TaskStyleFestive      = TaskStyle(12)
	TaskStyleBaroque      = TaskStyle(13)
	TaskStyleEtching      = TaskStyle(14)
	TaskStyleSDali        = TaskStyle(15)
	TaskStyleWuhtercuhler = TaskStyle(16)
	TaskStyleProvenance   = TaskStyle(17)
	TaskStyleRoseGold     = TaskStyle(18)
	TaskStyleMoonwalker   = TaskStyle(19)
	TaskStyleBlacklight   = TaskStyle(20)
	TaskStylePsychedelic  = TaskStyle(21)
	TaskStyleGhibli       = TaskStyle(22)
	TaskStyleSurreal      = TaskStyle(23)
)
