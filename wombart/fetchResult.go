package wombart

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/pkg/errors"
)

// FetchTaskResult returns the urls of the generated images after the task is complete. The last url is the final image.
func (client *Client) FetchTaskResult(taskID string) ([]string, error) {
	client.mutex.RLock()
	defer client.mutex.RUnlock()
	for {
		req, err := http.NewRequest(
			http.MethodGet,
			fmt.Sprintf("https://app.wombo.art/api/tasks/%s", taskID),
			nil,
		)
		req.Header.Set("Authorization", fmt.Sprintf("bearer %s", client.idToken))
		if err != nil {
			return nil, errors.Wrap(err, "create request")
		}
		resp, err := wrapResponse(client.httpClient.Do(req))
		if err != nil {
			return nil, errors.Wrap(err, "send request")
		}
		result := struct {
			URLs   []string    `json:"photo_url_list"`
			Result interface{} `json:"result"`
		}{}
		if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
			return nil, errors.Wrap(err, "unmarshal body")
		}
		if result.Result != nil {
			return result.URLs, nil
		}
		time.Sleep(2 * time.Second)
	}
}
