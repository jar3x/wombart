package wombart

import (
	"io"
	"net/http"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
)

func (client *Client) DownloadNewImage(spec TaskSpec, p string) error {
	urls, err := client.FetchNewImage(spec)
	if err != nil {
		return errors.Wrap(err, "fetch image")
	}
	if len(urls) == 0 {
		return errors.New("no images returned")
	}
	resp, err := wrapResponse(http.Get(urls[len(urls)-1]))
	if err != nil {
		return errors.Wrap(err, "fetch generated image")
	}
	if err := os.MkdirAll(filepath.Dir(p), 0755); err != nil {
		return errors.Wrap(err, "create directory")
	}
	file, err := os.OpenFile(p, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return errors.Wrap(err, "open file")
	}
	defer func(file *os.File) { _ = file.Close() }(file)

	if _, err := io.Copy(file, resp.Body); err != nil {
		return errors.Wrap(err, "write to file")
	}

	return nil
}
