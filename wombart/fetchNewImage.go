package wombart

import "github.com/pkg/errors"

func (client *Client) FetchNewImage(spec TaskSpec) ([]string, error) {
	taskID, err := client.createTaskID()
	if err != nil {
		return nil, errors.Wrap(err, "create task")
	}
	if err := client.StartTask(taskID, spec); err != nil {
		return nil, errors.Wrap(err, "start task")
	}
	urls, err := client.FetchTaskResult(taskID)
	if err != nil {
		return nil, errors.Wrap(err, "fetch result")
	}

	return urls, nil
}
