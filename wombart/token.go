package wombart

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/pkg/errors"
)

func (client *Client) refreshIDToken() error {
	token, err := client.fetchIDToken()
	if err != nil {
		return errors.Wrap(err, "fetch token")
	}
	client.mutex.Lock()
	defer client.mutex.Unlock()
	client.idToken = token

	return nil
}

func (client *Client) fetchIDToken() (string, error) {
	req, err := http.NewRequest(
		http.MethodPost,
		"https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDCvp5MTJLUdtBYEKYWXJrlLzu1zuKM6Xw",
		strings.NewReader("{\"returnSecureToken\":true}"),
	)

	if err != nil {
		return "", errors.Wrap(err, "create request")
	}
	resp, err := wrapResponse(client.httpClient.Do(req))
	if err != nil {
		return "", errors.Wrap(err, "send request")
	}
	result := struct {
		IDToken string `json:"idToken"`
	}{}
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return "", errors.Wrap(err, "unmarshal body")
	}

	return result.IDToken, nil
}
