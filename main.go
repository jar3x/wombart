package main

import (
	"fmt"
	"math/rand"
	"path"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/jar3x/wombart/wombart"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	client, err := wombart.NewClient()
	if err != nil {
		panic(err)
	}

	prompt := ""
	promptName := strings.ReplaceAll(prompt, " ", "_")
	specs := map[string]wombart.TaskSpec{}
	for i := 1; i < 24; i++ {
		for j := 0; j < 8; j++ {
			specs[path.Join(promptName, fmt.Sprintf("%02d", i), fmt.Sprintf("%s_%02d_%02d.jpg", promptName, i, j))] = wombart.TaskSpec{
				Prompt:           prompt,
				Style:            wombart.TaskStyle(i),
				DisplayFrequency: 10,
			}
		}
	}
	wg := sync.WaitGroup{}
	for p, spec := range specs {
		wg.Add(1)
		spec := spec
		p := p
		go func() {
			defer wg.Done()
			if err := client.DownloadNewImage(spec, p); err != nil {
				fmt.Println(errors.Wrap(errors.Wrap(err, "download image"), p))
				return
			}
			fmt.Printf("%s: download complete\n", p)
		}()
		time.Sleep(2*time.Second + time.Duration(rand.Intn(int(time.Second))))
	}
	wg.Wait()
	fmt.Println("all downloads complete")
}
